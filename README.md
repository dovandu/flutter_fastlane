# flutter_fastlane

Short Guide setup fastlane into flutter project

## source:
- https://appditto.com/blog/automate-your-flutter-workflow

## step by step
### Step 1: install fastlane
1 using gem
```sh
gem install fastlane
```
2  using Homebrew
```sh
brew install fastlane
```
### Step 2: setup gitignore
```sh
# You definitely want these
**/ios/*.ipa
**/ios/*.app.dSYM.zip

# These are optional
# For our open source projects we choose to exclude some metadata that contains private information
android/fastlane/report.xml
ios/fastlane/report.xml
ios/fastlane/Preview.html
ios/fastlane/metadata/review_information
ios/fastlane/metadata/trade_representative_contact_information
```
### Step3: Fastlane install 
1. for Android builds and deployments
```terminal
# cd android
# fastlane init
# cd ..
```
2. for IOS builds and deployments
```terminal
# cd ios
# fastlane init
# echo 'gem "cocoapods"' >> Gemfile
# cd ..
```
*** File setup into folder Android and ios

```sh
chmod a+x android/fastlane/flutter_build.sh
chmod a+x android/fastlane/flutter_test.sh

chmod a+x ios/fastlane/flutter_build.sh
chmod a+x ios/fastlane/flutter_test.sh

```


### Step 4: Installing the GitLab Runner
https://appditto.com/blog/automate-your-flutter-workflow#:~:text=Installing%20the%20GitLab%20Runner
1. setup gitlap runnrer
```terminal
# sudo curl --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-amd64
# sudo chmod +x /usr/local/bin/gitlab-runner
```
2. Run 
```terminal
# gitlab-runner install
# gitlab-runner start
```

3. Register
```terminal
gitlab-runner register --non-interactive --executor 'shell' --url 'https://gitlab/' --registration-token 'token'

gitlab-runner restart

gem install bundler:2.2.22

```



### Step 5: create .gitlab-ci.yml and run it

***
# Thanks for reading!
